const express = require('express');
const app = express();
require('./models/dbConfig');
const postRoutes = require('./routes/postsController');
const bodyParser = require('body-parser');
const moogoose = require('mongoose');
moogoose.set('useFindAndModify', false);
const cors = require('cors');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(cors());
app.use('/posts', postRoutes);

app.listen(5500, ()=> console.log('server started: 5500'));