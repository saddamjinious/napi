const express = require('express');
const {ObjectID} = require("bson");
const router= express.Router();

const {PostsModel} = require('../models/postsModel');

router.get('/',(req,res)=>{
    PostsModel.find((err,docs)=>{
       if(!err) res.send(docs)
       else console.log("il y'a une erreur :" +err)
    })
} )

router.post('/',(req,res)=>{
   const newRecord = new PostsModel({
       author:req.body.author,
       message:req.body.message
   });
   newRecord.save((err,docs)=>{
       if(!err) res.send(docs);
       else console.log('error creating new data : ' + err)
   })
})
router.put('/:id',(req,res)=>{
    if(!ObjectID.isValid(req.params.id))
        return res.status(400).send("ID unknow :"+ req.params.id)
    const updateRecord = {
        author: req.body.author,
        message:req.body.message

    };
    PostsModel.findByIdAndUpdate(
        req.params.id,
        {$set: updateRecord},
        {new: true},
        (err,docs)=>{
            if(!err) res.send(docs);
            else console.log("Update error : " + err);
        }
    )
})

router.delete('/:id',(req,res)=>{
    if(!ObjectID.isValid(req.params.id))
        return res.status(400).send("ID unknow :"+ req.params.id)
    PostsModel.findByIdAndRemove(
        req.params.id,
        (err,docs)=>{
        if(!err)res.send(docs);
        else console.log("delete error :" + err);

    })
});
module.exports = router;