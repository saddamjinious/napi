FROM node:12.2.0
RUN apt-get -y update
#&& apt-get install -y yarn
#RUN curl -o- -L https://yarnpkg.com/install.sh | bash
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json /app/package.json
RUN rm -f package-lock.json && \
    npm install
COPY . /app
CMD ["npm","start"]
